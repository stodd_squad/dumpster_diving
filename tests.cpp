#include <iostream>
#include <string>

#include "lib/util.h"

int main() {
	std::cout << "Begin Tests" << std::endl;
	Util::move(Util::getFullPath("test_dir"), Util::getFullPath("test_directory.1"));
	Util::move(Util::getFullPath("test_directory.1"), Util::getFullPath("test_dir"));
	Util::destroy(Util::getFullPath("test_dir"));
}