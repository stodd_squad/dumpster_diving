//
// Created by ccormier on 3/19/18.
//

#include <iostream>
#include "lib/util.h"

int main(){
    // get and check dumpster location
    std::string strDumpDir = Util::getDumpDir();
    if(Util::isValidDump(strDumpDir)){
        return 1;
    }

    for(const auto& file: Util::getDirContents(strDumpDir)){
        std::string fullPath = strDumpDir + "/"  + file;
        if(Util::destroy(fullPath)){
            std::cerr << "Error dumping file \"" << file << "\"" << std::endl;
        }
    }
}
