//
// Created by ccormier on 3/19/18.
//

#include <iostream>
#include <getopt.h>
#include "lib/util.h"

void usage(){
    std::cerr << "Usage: rm [-h|-q|-r] [-t] FILES..." << std::endl;
    std::cerr << "Restore FILES from dumpster (denoted by environment variable DUMPSTER)"
              << " into the current working directory." << std::endl;
    std::cerr << "-h\tShow this dialog." << std::endl;
    std::cerr << "-l\tList all files in the dumpster." << std::endl;
    std::cerr <<   "\tIf no files are specified, lists all files and folders in dumpster." << std::endl;
    std::cerr << "-r\tRecursively restore files from dumpster." << std::endl;
    std::cerr << "-t\tRestore files with truncated names, otherwise the full name " << std::endl;
    std::cerr <<   "\tof the dumpster file will be the name of the new file." << std::endl;
}

int main(int argc, char* argv[]){
    int c;
    bool lflag=false, rflag=false, tflag=false, errflag=false;
    char *ofile = nullptr, *ifile = nullptr;

    opterr = 1; /* set to 0 to disable error message */

    // process command line arguments
    while ((c = getopt (argc, argv, "hlrt")) != EOF) {
        switch (c){
            case 'h':
                errflag = true;
                break;
            case 'l':
                lflag = true;
                break;
            case 'r':
                rflag = true;
                break;
            case 't':
                tflag = true;
                break;
            default:
                errflag = true;
                break;
        }
    }

    std::vector<std::string> files = Util::getFiles(argc, argv, optind);

    // check if command line arguments were processed properly
    if((lflag && rflag) || errflag){
        usage();
        return 1;
    }

    if(files.empty()){
        std::cout << "No files listed," << std::endl;
        std::cout << "Try 'dv -h' for more information," << std::endl;
        return 1;
    }

    // get and check dumpster location
    std::string strDumpDir = Util::getDumpDir();
    if(Util::isValidDump(strDumpDir)){
        return 1;
    }

    if(lflag) {
        // list files in dump
    }else{
        for(const auto& file: files){
            std::string dumpFile = strDumpDir+"/"+file;
            // skip if we cannot stat file
            if(!Util::fileExists(dumpFile)){
                std::cout << "Skipping restoration of \"" << file
                          << "\" due to it not existing." << std::endl;
                continue;
            }

            // skip if directory and -r option is not used
            if( Util::isDir(dumpFile) && !rflag){
                std::cout << "Skipping restoration of directory \"" << file
                          << "\" due to -f option not being used " << std::endl;
                continue;
            }

            // otherwise restore files
            if(Util::inDir(strDumpDir, file)){
                std:string newFile = file;
                // restore with shortened name
                if(tflag){
                    newFile = Util::removeExtraFileExtension(newFile);
                }
                newFile = Util::getFullPath(newFile);
                Util::move(dumpFile, Util::getFullPath(newFile));
            }else{
                std::cout << "Not restoring file \"" << file
                          << "\" because it does not in dumpster." << std::endl;
            }
        }
    }
}
