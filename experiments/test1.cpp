#include <iostream>
#include <istream>
#include <string>
#include <vector>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/time.h>

using std::string;

int main() {
	std::cout << "---Experiment 1: Timing rename()---" << std::endl;
	std::cout << "---Name the file to rename---" << std::endl;
	string fileName, newName;
	std::cin >> fileName;

	std::cout << "Number of times to rename it?" << std::endl;
	int loopCount;
	std::cin >> loopCount;
	
	int len = fileName.length();
	newName = fileName + ".0";
	struct timeval start, end;

	std::vector<string> fNames = {fileName};
	fNames.push_back(newName);
	for(int i = 0; i < loopCount; i++) {
		newName.replace(len, string::npos, "."+std::to_string(i+1));
		fNames.push_back(newName);
		std::cout << newName << std::endl;
	}

	gettimeofday(&start, NULL);
	
	for(int i = 0; i < loopCount; i ++) {
		int rc = rename(fNames[i].c_str(), fNames[i+1].c_str());
		if(rc) std::cout << strerror(errno) << std::endl;
	}
	sync();
	
	gettimeofday(&end, NULL);
	long int time = ((end.tv_sec - start.tv_sec) * 1e6 + (end.tv_usec - start.tv_usec));
	std::cout << "---Experiment finished in " + std::to_string(time) + " usec" << std::endl;
	std::cout << "Avg time: " + std::to_string(time / loopCount) << std::endl;
}