#include <iostream>
#include <istream>
#include <sys/time.h>
#include <string>
#include "../lib/util.h"

int main() {
	std::cout << "Experiment 3" << std::endl;
	std::cout << "Enter full path to directory to remove";
	string dir;
	std::cin >> dir;
	struct timeval start, end;
	gettimeofday(&start,NULL);
	Util::destroy(dir);
	gettimeofday(&end,NULL);
	long int time = ((end.tv_sec - start.tv_sec) * 1e6 + (end.tv_usec - start.tv_usec));
	std::cout << "Time (usec): " + std::to_string(time) << std::endl;
}