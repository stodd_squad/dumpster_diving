#include <iostream>
#include <istream>
#include <string>
#include <unistd.h>
#include <sys/time.h>
#include "../lib/util.h"

using std::string;

int main() {
	std::cout << "---Experiment 2---" << std::endl;
	std::cout << "---File to be copied---" << std::endl;
	string inName;
	std::cin >> inName;
	string outName;
	std::cout << "---To be copied to---" << std::endl;
	std::cin >> outName;
	struct stat buf;
   if(stat(inName.c_str(), &buf)) {
      std::cout << "stat() failed" << std::endl;
      return 1;
   }
	struct timeval start, end;
	gettimeofday(&start, NULL);
	Util::copyFile(inName, outName);
	sync();
	gettimeofday(&end, NULL);

	long int bitrate = buf.st_size / ((end.tv_sec - start.tv_sec) * 1e6 + (end.tv_usec - start.tv_usec));
	std::cout << "Bitrate (bytes / usec):" + std::to_string(bitrate) << std::endl;
}