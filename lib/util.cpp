#define DEBUG true
#include "util.h"
#include <fstream>
#include <cstring>
#include <dirent.h>
#include <vector>
/*For dirname and basename*/
#include <libgen.h>
/*For stat (getting file status*/
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
/*For chmod*/
#include <fcntl.h> //definition of AT_ constants
/*For utime*/

#include <utime.h>

#include "util.h"

// struct stat {
//                dev_t     st_dev;         /* ID of device containing file */
//                ino_t     st_ino;         /* inode number */
//                mode_t    st_mode;        /* protection */
//                nlink_t   st_nlink;       /* number of hard links */
//                uid_t     st_uid;         /* user ID of owner */
//                gid_t     st_gid;         /* group ID of owner */
//                dev_t     st_rdev;        /* device ID (if special file) */
//                off_t     st_size;        /* total size, in bytes */
//                blksize_t st_blksize;     /* blocksize for filesystem I/O */
//                blkcnt_t  st_blocks;      /* number of 512B blocks allocated */

//                /* Since Linux 2.6, the kernel supports nanosecond
//                   precision for the following timestamp fields.
//                   For the details before Linux 2.6, see NOTES. */

//                struct timespec st_atim;  /* time of last access */
//                struct timespec st_mtim;  /* time of last modification */
//                struct timespec st_ctim;  /* time of last status change */

//            #define st_atime st_atim.tv_sec      /* Backward compatibility */
//            #define st_mtime st_mtim.tv_sec
//            #define st_ctime st_ctim.tv_sec
//            };
using std::string;
namespace Util {
    /**
     * @param source: the source path (relative to current directory unless / specified)
     * @param dest : the destination path (relative to current directory unless / specified)
     * @return 0 on success, 1 otherwise
     * Copies a file or directory from source to dest
     */
    int move(string source, string dest) {
        using std::fstream;
        if(DEBUG) std::cout << "---Begin Copy---" << std::endl;
        /*Get destination dir name*/
        auto * cp = new char [dest.length() + 1];
        std::strcpy(cp, dest.c_str());
        std::string destDir(dirname(cp));
        std::string destPath(getFullPath(destDir));
        delete[] cp;
        
        /*Get destination file metadata*/
        struct stat destBuf{};
        if(stat(destPath.c_str(), &destBuf)) {
            if(DEBUG) std::cout << "move dest: stat() failed on \"" << destPath
                                << "\": " << strerror(errno) << std::endl;
            return 1;
        }
        /*Get source file metadata*/
        struct stat buf{};
        if(stat(source.c_str(), &buf)) {
            if(DEBUG) std::cout << "source stat() failed on \"" << source
                                << "\": " << strerror(errno) << std::endl;
            return 1;
        }

        /*Check that source and dest are on the same partition*/
        if(destBuf.st_dev == buf.st_dev) {
            rename(source.c_str(), dest.c_str());
            return 0;
        }

        //SEPERATE PARTITION
        int ret = copyRecursiveHook(source, dest);
        if(!ret){
            destroy(source);
        }
        if(DEBUG) std::cout << "---Copy Completed---" << std::endl;
        return ret;
    }

    int copyRecursiveHook(const std::string &source, const std::string& dest){
        /*Get source file metadata*/
        struct stat buf{};
        if(stat(source.c_str(), &buf)) {
            if(DEBUG) std::cout << "copyRecursiveHook stat() failed on \"" << source
                                << "\": " << strerror(errno) << std::endl;
            return 1;
        }

        if(buf.st_mode & S_IFDIR) {
            if(DEBUG) std::cout << "---Copy called on directory---" << std::endl;
            return copyDirectory(source, dest);
        }else {
            if(DEBUG) std::cout << "---Copy called on file---" << std::endl;
            return copyFile(source, dest);
        }
        if(DEBUG) std::cout << "---Copy Completed---" << std::endl;
        return 0;
    }

    /**
     * [copyDirectory description]
     * @param  source [description]
     * @param  dest   [description]
     * @return        [description]
     */
    int copyDirectory(string source, string dest) {
        if(DEBUG) std::cout << "---Copy Directory " << source << " -> " << dest << std::endl;
        //Get MetaData
        struct stat buf{};
        if(stat(source.c_str(), &buf)) {
            if(DEBUG) std::cout << "stat() failed" << std::endl;
            return 1;
        }
        //Make directory at destination
        mkdir(dest.c_str(), buf.st_mode);
        //Search source for files an directories
        auto dirCont = getDirContents(source);
        for(const auto& name : dirCont) {
            return copyRecursiveHook(source + "/" + name, dest + "/" + name);
        }
    }

    int copyFile(string source, string dest) {
        struct stat buf{};
        if(stat(source.c_str(), &buf)) {
            if(DEBUG) std::cout << "copyFile: stat() failed\"" << dest
                                << "\": " << strerror(errno) << std::endl;
            return 1;
        }
        /*Copy source to dest*/
        std::ifstream f1 (source, std::fstream::binary);
        std::ofstream f2 (dest, std::fstream::trunc|std::fstream::binary);
        f2 << f1.rdbuf();
        /*Copy metadata*/
        chown(source.c_str(), buf.st_uid, buf.st_gid); //User and Group ID
        chmod(source.c_str(), buf.st_mode);
        struct utimbuf times{};
        times.actime = buf.st_atim.tv_sec;
        times.modtime = buf.st_mtim.tv_sec;
        utime(source.c_str(), &times);
        return 0;
    }

    /**
     * @param rPath: the path of the file/directory to be removed (relative unless / specified)
     * @return 0 on success, 1 otherwise
     */
    int destroy(string rPath) {
        if(DEBUG) std::cout << "---Begin Remove---" << std::endl;
        string fullPath(getFullPath(rPath));
        if(remove(fullPath.c_str())){
            if(errno == ENOTEMPTY) {
                if(DEBUG) std::cout << "Found full directory" << std::endl;
                for(const auto& name : getDirContents(fullPath)) {
                    destroy(fullPath + '/' + name);
                }
                remove(fullPath.c_str());
            }else{
                std::cout << "Remove error " << strerror(errno) << std::endl;
                return 1;
            }
        }

        return 0;
    }

    std::string getFullPath(const std::string& str_original){
        if(str_original[0] != '/'){
            char cwd[1024];
            getcwd(cwd, sizeof(cwd));
            std::string string_cwd = cwd;
            return string_cwd.append("/").append(str_original);
        }else{
            return str_original;
        }
    }

    std::string getDumpDir(){
        char* dir = getenv("DUMPSTER");
        if(dir == nullptr){
            std::cerr << "DUMPSTER enviroment variable not set" << std::endl;
            return "";
        }else if(dir[0] != '/'){
            std::cerr << "DUMPSTER enviroment variable \"" << dir
                      << "\"is not absolute." << std::endl;
            return "";
        }else{
            return dir;
        }
    }

    // TODO: make this an iterator?
    // That way we don't need to store all files
    std::vector<std::string> getDirContents(const std::string& str_dir){
        std::vector<std::string> dirContents;
        DIR *dir;
        struct dirent *ent;
        if((dir = opendir(str_dir.c_str())) != nullptr){
            while ((ent = readdir(dir)) != nullptr){
                std::string name = ent->d_name;
                if(name != "." && name != ".."){
                    dirContents.emplace_back(name);
                }
            }
            closedir(dir);
        }else{
            std::cerr << "Could not open directory " << str_dir << std::endl;
        }
        return dirContents;
    }

    bool inDir(const std::string& str_dir, const std::string& str_file){
        DIR *dir;
        struct dirent *ent;
        bool ret = false;
        if((dir = opendir(str_dir.c_str())) != nullptr){
            while ((ent = readdir(dir)) != nullptr){
                if(str_file == ent->d_name){
                    return true;
                }
            }
            closedir(dir);
        }else{
            std::cerr << "Could not open directory " << str_dir << std::endl;
        }
        return false;
    }

    std::string removeExtraFileExtension(const std::string& oldString){
        size_t lastindex = oldString.find_last_of('.');
        if(lastindex != std::string::npos){
            std::string extension = oldString.substr(lastindex, oldString.length());
            // check if char is number between 1 and 9
            if(('1' <= extension[1] || extension[1]<= '9') && extension.length() == 2){
                return oldString.substr(0, lastindex);
            }

        }
        return oldString;
    }

    bool isDir(const std::string &file){
        struct stat buf{};
        if(stat(file.c_str(), &buf)) {
            if(DEBUG) std::cout << "isDir stat() failed on \"" << file
                                << "\": " << strerror(errno) << std::endl;
            return false;
        }
        return (bool) (buf.st_mode & S_IFDIR);
    }

    std::vector<std::string> getFiles(int argc, char* argv[], int optind){
        std::vector<std::string> files;
        int filenum = 1;
        while(optind < argc){
            std::string file = argv[optind];
            files.emplace_back(file);
            filenum++;
            optind++;
        }
        return files;
    }
}