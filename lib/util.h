#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <cstring>
#include <iostream>
#include <vector>
#include <sys/stat.h>
/*For dirname and basename*/
#include <libgen.h>

using std::string;
namespace Util {
    std::vector<std::string> getFiles(int argc, char* argv[], int optind);
    int move(string source, string dest);
    int destroy(string rPath);
    int copyRecursiveHook(const std::string &source, const std::string& dest);
    int copyDirectory(string source, string dest);
    int copyFile(string source, string dest);
    std::string getFullPath(const std::string& str_original);
    std::string getDumpDir();
    bool inDir(const std::string& str_dir, const std::string& str_file);
    bool isDir(const std::string &file);
    std::vector<std::string> getDirContents(const std::string& str_dir);
    std::string removeExtraFileExtension(const std::string& oldString);

    inline bool isValidDump(const std::string& dump){
        //check if dump string is a absolute path
        if(dump.empty() || (dump[0] != '/')){
            std::cerr << "DUMPSTER enviroment variable is set to \""
                      << dump << "\" it should be a absolute file path" << std::endl;
            return false;
        }

        // check that if it exists and is a directory
        if(!isDir(dump)){
            std::cerr << "DUMPSTER enviroment variable is set to \""
                      << dump << "\" which is not an already created directory" << std::endl;
            return false;
        }
    }

    inline bool fileExists(const string &file){
        struct stat buf{};
        return !(bool)(stat(file.c_str(), &buf));
    }

    inline string getFileName(const string& fullPath){
        auto * cp = new char [fullPath.length() + 1];
        strcpy(cp, fullPath.c_str());
        string fileName(basename(cp));
        delete[] cp;
        return fileName;
    }
};
#endif