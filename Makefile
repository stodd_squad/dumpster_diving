CC=g++
CFLAGS=-std=c++11

all: dump_main dv_main rm_main

dump_main: dump_main.o util.o
	$(CC) $(CFLAGS) -o dump dump_main.o util.o
dv_main: dv_main.o util.o
	$(CC) $(CFLAGS) -o dv dv_main.o util.o
rm_main: rm_main.o util.o
	$(CC) $(CFLAGS) -o rm rm_main.o util.o
dv_main.o: dv_main.cpp util.o
	$(CC) $(CFLAGS) -c dv_main.cpp
rm_main.o: rm_main.cpp util.o
	$(CC) $(CFLAGS) -c rm_main.cpp
dump_main.o: dump_main.cpp util.o
	$(CC) $(CFLAGS) -c dump_main.cpp
util.o: lib/util.h lib/util.cpp
	$(CC) $(CFLAGS) -c lib/util.cpp
clean:
	rm -f rm dv dump rm_main.o dv_main.o dump_main.o util.o