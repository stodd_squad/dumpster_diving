# Dumpster Diving - CS 4513 Project 1
_Chris Cormier and Turner Robbins_

This project implements a "dumpster" that files are moved to when they are deleted.
The dumpster is specified by the environment variable "DUMPSTER" and must be an
absolute file path to an existing directory. All operations in this application 
expect that the dumpster exists beforehand.

##rm
"Deletes" a file by moving it to the dumpster. If a file of the same name already
exists, the file is appended with .#. Where # is a number 1-9
###Usage: rm [-h|-f|-r]... FILE...
Delete and move FILES to the dumpster (denoted by environment variable DUMPSTER)

-h    Show the usage dialog.

-f    Complete deletion, does not move files to dumpster

-r    Delete directory recursively

##dv
Restores a file from the dumpster in the current directory. The restored files
are named the same as they were in the dumpster.
###Usage: Usage: rm [-h|-q|-r] [-t] FILES...
Restore FILES from dumpster into the current working directory.

-h    Show the usage dialog.

-l    List all files in the dumpster. If no files are specified, lists all files and folders in dumpster.

-r    Recursively restore files from dumpster.

-t    Restore files with truncated names, otherwise the full name of the dumpster file will be the name of the new file.
##dump
Deletes the contents of the dumpster. There are no options 
associated with this operation.
