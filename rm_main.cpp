//
// Created by ccormier on 3/19/18.
//

#include <iostream>
#include <sstream>
#include <getopt.h>
#include "lib/util.h"

void usage(){
    std::cerr << "Usage: rm [-h|-f|-r]... FILE..." << std::endl;
    std::cerr << "Delete and move FILES to the dumpster (denoted by environment variable DUMPSTER)" << std::endl;
    std::cerr << "-h\tShow this dialog." << std::endl;
    std::cerr << "-f\tComplete deletion, does not move files to dumpster" << std::endl;
    std::cerr << "-r\tDelete directory recursively" << std::endl;
}

std::string getDumpsterFileName(const std::string &name, std::string dump){
    std::string testFile(Util::getFileName(name));
    uint8_t  nCount = 0;
    while(Util::inDir(dump, testFile)){
        if(nCount == 9){
            std::cerr << "Too many items in dumpster with name \""
                      << name << "\" skipping deletion. "
                      <<"Please empty dumpster or force deletion." << std::endl;
            return "";
        }

        nCount += 1;
        testFile = Util::getFileName(name);
        testFile.append(".");
        testFile.append(std::to_string(nCount));
    }
    dump.append("/").append(testFile);
    return dump;
}

int main(int argc, char* argv[]){
    int c;
    bool fflag=false, rflag=false, errflag=false;
    char *ofile = nullptr, *ifile = nullptr;

    opterr = 1; /* set to 0 to disable error message */

    while ((c = getopt (argc, argv, "hfr")) != EOF) {
        switch (c){
            case 'h':
                errflag = true;
               break;
            case 'f':
                fflag = true;
                break;
            case 'r':
                rflag = true;
                break;
            default:
                errflag = true;
                break;
        }
    }
    std::vector<std::string> files = Util::getFiles(argc, argv, optind);

    if((fflag && rflag) || errflag){
        usage();
        return -1;
    }
    if(files.empty()){
        std::cout << "No files listed" << std::endl;
        std::cout << "Try 'dv -h' for more information," << std::endl;
    }

    // get and check dumpster location
    std::string strDumpDir = Util::getDumpDir();
    if(Util::isValidDump(strDumpDir)){
        return 1;
    }

    for(const auto& file : files){
        // skip if we cannot stat file
        if(!Util::fileExists(file)){
            std::cout << "Skipping deletion of \"" << file
                      << "\" due to it not existing." << std::endl;
            continue;
        }

        // skip if file is directory and -r option not used
        if( Util::isDir(file) && !rflag){
            std::cout << "Skipping deletion of directory \"" << file
                      << "\" due to -r option not being used " << std::endl;
            continue;
        }

        if(fflag){
            // just delete the file
            if(Util::destroy(file)){
                std::cerr << "Error deleting file \"" << file << "\"" << std::endl;
            }
            continue;
        }

        // get dumpster file name
        std::string newFile = getDumpsterFileName(file, strDumpDir);
        if(newFile.empty()){
            break;
        }

        // move
        if(Util::move(Util::getFullPath(file), newFile)){
            std::cerr << "copying file from:\"" << file << "\" to \""
                      << newFile << "\" failed." << std::endl;
        }
    }
}
